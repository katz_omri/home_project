import { Injectable } from "@angular/core";
import { HttpClient } from "@angular/common/http";
import { map, tap } from "rxjs/operators";
import { map as rMap } from "ramda";

/**
 * id: "6576013151"
 owner: "45389964@N06"
 secret: "22dbc710b3"
 server: "7166"
 farm: 8
 title: "Siberian Husky"
 ispublic: 1
 isfriend: 0
 isfamily: 0
 * @param photo
 */

// https://farm1.staticflickr.com/2/1418878_1e92283336_m.jpg
//
// farm-id: 1
// server-id: 2
// photo-id: 1418878
// secret: 1e92283336
// size: m

interface Photo {
    id: string;
    secret: string;
    server: string;
    farm: number;
}
const buildUrl = ({id, secret, server, farm, ...rest}: Photo) => {
    return { url: `https://farm${farm}.staticflickr.com/${server}/${id}_${secret}_m.jpeg`, ...rest };
};

@Injectable({
    providedIn: "root"
})
export class FlickerService {
    baseUrl =
        "https://api.flickr.com/services/rest/?method=flickr.photos.search&safe_search=1&format=json&nojsoncallback=1&api_key=15b67c2a8b4288ff1fddf5eb56655cfb&content_type=1&is_getty=1‌‌";
    constructor(private http: HttpClient) {}

    searchImages(text: string, page = 0, perPage = 20) {
        return this.http.get(`${this.baseUrl}&text=${encodeURIComponent(text)}&per_page=${perPage}&page=${page}`).pipe(
            map(({ photos }: any) => photos.photo),
            map(rMap(buildUrl))
        );
    }
}
