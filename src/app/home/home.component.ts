import { AfterViewInit, Component, ElementRef, OnDestroy, OnInit, ViewChild } from "@angular/core";
import { FlickerService } from "../flicker.service";
import { MatButton } from "@angular/material/button";
import { BehaviorSubject, forkJoin, fromEvent, Observable, Subscription } from "rxjs";
import { auditTime, debounceTime, distinctUntilChanged, filter, first, map, switchMap, tap } from "rxjs/operators";
import { FormControl } from "@angular/forms";
import { flatMap } from "tslint/lib/utils";
import { flatten } from "ramda";
import { CdkVirtualScrollViewport } from "@angular/cdk/scrolling";

enum Condition {
    And = "and",
    Or = "or"
}

@Component({
    selector: "app-home",
    templateUrl: "./home.component.html",
    styleUrls: ["./home.component.scss"]
})
export class HomeComponent implements OnInit, AfterViewInit, OnDestroy {
    constructor(private flickerService: FlickerService) {}
    public Condition = Condition;
    @ViewChild("inputElement") button: ElementRef<HTMLInputElement>;

    ngOnInit(): void {}

    private _images = new BehaviorSubject<any[]>([]);

    private _page = new BehaviorSubject<number>(0);
    $page = this._page.asObservable();

    $images = this._images.asObservable();

    onScroll() {
        this._page.next(this._page.getValue() + 1);
        this.onSearchHistory();
    }

    condition: Condition = Condition.And;

    static STORAGE_KEY = "search_history";

    onSearchHistory() {
        if (!this.searchHistory.value) {
            return;
        }
        if (this.condition === Condition.And) {
            const term = this.searchHistory.value.join("+");
            this.subscriptions.push(
                this.$page
                    .pipe(
                        switchMap(page => {
                            return this.flickerService.searchImages(term, page).pipe(
                                tap(this._images),
                                first()
                            );
                        })
                    )
                    .subscribe()
            );
            return;
        }

        forkJoin(...this.searchHistory.value.map(term => this.flickerService.searchImages(term)))
            .pipe(first())
            .subscribe(results => {
                this._images.next(flatten(results));
            });
    }

    onSaveSearch(searchTerm: string) {
        if (!searchTerm) {
            return;
        }
        let current = [];
        try {
            current = JSON.parse(localStorage.getItem(HomeComponent.STORAGE_KEY)) || [];
        } catch (e) {
            // ignore errors
        }
        localStorage.setItem(HomeComponent.STORAGE_KEY, JSON.stringify([...current, searchTerm]));
        setTimeout(() => {}, 1000);
    }

    onClick(event) {
        this._images.next([]);
        this.value = "";
    }
    private _value;
    set value(val) {
        this.button.nativeElement.value = val;
        this._value = val;
    }
    get value() {
        return this._value;
    }
    subscriptions: Subscription[] = [];

    searchHistory = new FormControl();

    searchHistoryList: any[] = JSON.parse(localStorage.getItem(HomeComponent.STORAGE_KEY)) || [];

    searchTerm = "";

    ngAfterViewInit(): void {
        this.subscriptions.push(
            fromEvent(this.button.nativeElement, "keyup")
                .pipe(
                    map((e: any) => e.target.value),
                    filter(value => !!value.length),
                    tap(value => (this.value = value)),
                    debounceTime(100),
                    distinctUntilChanged(),
                    tap(term => (this.searchTerm = term)),
                    switchMap(term => this.$page.pipe(map(page => ({ term, page })))),
                    switchMap(({ term, page }) => this.flickerService.searchImages(term, page)),
                    tap(images => this._images.next([...this._images.getValue(), ...images]))
                )
                .subscribe()
        );
    }

    ngOnDestroy(): void {
        if (!this.subscriptions.length) {
            return;
        }
        this.subscriptions.forEach(sub => sub.unsubscribe());
    }
}
